# Enhanced Super Resolution Generative Adversarial Network (ESRGAN) Model for Satellite Imagery


## Testing the model on colab

1. Open the jupyter_notebooks\ESRGAN_JupyterNotebook.ipynb in google colab by clicking on the 'Open in Colab' button on the top. 

2. Generate the Kaggle API Token as explained in the Markdown inside the jupyter notebook and upload it when colab prompts you to upload a file.
3. Execute each cell to run the AI model and generate the outputs accordingly. 


# **FOLLOW THE ABOVE STEPS FOR TESTING THE MODEL ON GOOGLE COLAB!!!**
## Environment Setup

Install Anaconda Environment for Linux by following the steps from [here](https://docs.anaconda.com/anaconda/install/linux/). 

- After installing the anaconda, go ahead and create an anaconda environment
```
conda create --name myenv
```
- 'myenv' can be any name. Type y to proceed

- Activate the created environment
```
conda activate myenv
```
- After creating the virtual environment, navigate to the directory where the requirements.txt is located and then install the dependencies 

```
pip install -r requirements.txt
```
## Dataset
The dataset used to develop the current AI model is from [kaggle](https://www.kaggle.com/balraj98/deepglobe-road-extraction-dataset). 
- The dataset only consists of high resolution satellite images from various sources.
 
# Testing the AI model

## Pre-trained Model
- Import the pretrained generator model and test the model on your input low resolution images using the following bash command. 

## Single Image

```bash
python ESRGAN_Model_Test.py --arc=esrgan --lr_path=/content/dataset/lr/aa.jpg --save_dir=/content/Keras-Image-Super-Resolution/test_out --model_path=/content/Keras-Image-Super-Resolution/checkpoints/erca-10.h5 --cuda=0
```

**Note**: Please make sure that the destination directory is empty to ease your workflow. 

## Model OUTPUT:

![](/imgs/Test_Case_1.png)

![](/imgs/Test_Case_2.png)

![](/imgs/Test_Case_3.png)

<hr>

## Tensorflow Lite Model

- A lite version of the developed tensorflow model is attached in the pre-trained-model folder. 
- This lite model can be deployed directly on low power devices. 
- The tensorflow Lite model is higly efficient for low power devices such as RPis, Nvidia Jetson Nanos etc.

<hr>

## AI model Movidius 2 Compilation
- To compile the AI model for Movidius kindly refer the official documentation [here](https://movidius.github.io/ncsdk/tf_compile_guidance.html).
- For additional compilation on other variants kindly refer [this](https://www.dlology.com/blog/how-to-run-keras-model-on-movidius-neural-compute-stick/).







