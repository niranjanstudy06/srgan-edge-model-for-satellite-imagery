# Enhanced Super Resolution Generative Adversarial Network (ESRGAN) Model for Satellite Imagery



## Environment Setup

Install Anaconda Environment for Linux by following the steps from [here](https://docs.anaconda.com/anaconda/install/linux/). 

- After installing the anaconda, go ahead and create an anaconda environment
```
conda create --name myenv
```
- 'myenv' can be any name. Type y to proceed

- Activate the created environment
```
conda activate myenv
```
- After creating the virtual environment, navigate to the directory where the requirements.txt is located and then install the dependencies 

```
pip install -r requirements.txt
```
## Dataset
The dataset used to develop the current AI model is from [kaggle](https://www.kaggle.com/balraj98/deepglobe-road-extraction-dataset). 
- The dataset only consists of high resolution satellite images from various sources.
 
# Testing the AI model

## Pre-trained Model
- Import the pretrained generator model and test the model on your input low resolution images using the following bash command. 

## Single Image

```bash
python ESRGAN_Model_Test.py --arc=esrgan --lr_path=/content/dataset/lr/aa.jpg --save_dir=/content/Keras-Image-Super-Resolution/test_out --model_path=/content/Keras-Image-Super-Resolution/checkpoints/erca-10.h5 --cuda=0
```

**Note**: Please make sure that the destination directory is empty to ease your workflow. 


# Training
To pretrain a generator, run the following command

- Data folders should consist of a HR folder and a LR folder, e.g: data/train/sat_img/HR and data/train/sat_img/LR.

- To train a generator by using GAN, run the following command
```
python gantrain.py --arc=esrgan --train=/path/to/training/data --train-ext=.jpg --g_init=/path/to/pretrain/model --cuda=0
```




